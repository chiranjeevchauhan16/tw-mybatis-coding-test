package com.mybatis.demo.model;


public class User {

	private String id;
	private String createdBy;
	private String createdTime;
	private String lastUpdatedBy;
	private String lastUpdatedTime;
	private String firstName;
	private String lastName;
	private String picture;
	private String preferredLanguage;
	private long backgroundCheck;
	
	public User() {
		
	}
	
	public User(String id, String createdBy, String createdTime, String lastUpdatedBy, String lastUpdatedTime,
			String firstName, String lastName, String picture, String preferredLanguage, long backgroundCheck) {
		this.id = id;
		this.createdBy = createdBy;
		this.createdTime = createdTime;
		this.lastUpdatedBy = lastUpdatedBy;
		this.lastUpdatedTime = lastUpdatedTime;
		this.firstName = firstName;
		this.lastName = lastName;
		this.picture = picture;
		this.preferredLanguage = preferredLanguage;
		this.backgroundCheck = backgroundCheck;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(String lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getPreferredLanguage() {
		return preferredLanguage;
	}

	public void setPreferredLanguage(String preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}

	public long getBackgroundCheck() {
		return backgroundCheck;
	}

	public void setBackgroundCheck(long backgroundCheck) {
		this.backgroundCheck = backgroundCheck;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", createdBy=" + createdBy + ", createdTime=" + createdTime + ", lastUpdatedBy="
				+ lastUpdatedBy + ", lastUpdatedTime=" + lastUpdatedTime + ", firstName=" + firstName + ", lastName="
				+ lastName + ", picture=" + picture + ", preferredLanguage=" + preferredLanguage + ", backgroundCheck="
				+ backgroundCheck + "]";
	}
	
}
