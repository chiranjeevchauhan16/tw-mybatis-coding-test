package com.mybatis.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.mybatis.demo.model.User;
import com.mybatis.demo.repository.UserRepository;

@SpringBootApplication
public class SpringbootMybatisDemoApplication implements CommandLineRunner{

private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private UserRepository userRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringbootMybatisDemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		logger.info("Inserting -> {}", userRepository.insert(new User("100", "chiranjeev", "2020-05-18 13:10:11", "vikas", "2020-05-18 13:10:11", "chiranjeev", "chauhan", "1.jpg", "english", 2)));
		logger.info("Inserting -> {}", userRepository.insert(new User("101", "rohit", "2020-05-18 13:10:11", "vikas", "2020-05-18 13:10:11", "rohit", "singh", "1.jpg", "english", 3)));
		
		logger.info("user id 100 -> {}", userRepository.findById("100"));

		logger.info("Update 100 -> {}", userRepository.update(new User("100", "chiranjeev", "2020-05-18 13:10:11", "prashant", "2020-05-18 13:10:11", "chiranjeev", "chauhan", "1.jpg", "english", 2)));

		userRepository.deleteById("101");

		logger.info("All users -> {}", userRepository.findAll());
	}

}
