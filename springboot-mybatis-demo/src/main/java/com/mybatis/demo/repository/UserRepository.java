package com.mybatis.demo.repository;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.mybatis.demo.model.User;

@Mapper
public interface UserRepository {

	@Select("select * from users")
	public List<User> findAll();

	@Select("SELECT * FROM users WHERE id = #{id}")
	public User findById(String id);

	@Delete("DELETE FROM users WHERE id = #{id}")
	public int deleteById(String id);

	@Insert("INSERT INTO users(id ,createdBy,createdTime ,lastUpdatedBy ,lastUpdatedTime ,firstName,lastName,picture ,preferredLanguage ,backgroundCheck) "
			+ " VALUES (#{id}, #{createdBy}, #{createdTime}, #{lastUpdatedBy}, #{lastUpdatedTime}, #{firstName}, #{lastName}, #{picture}, #{preferredLanguage},#{backgroundCheck})")
	public int insert(User user);

	@Update("UPDATE users set createdBy=#{createdBy}, "
			+ " createdTime=#{createdTime}, "
			+ "lastUpdatedBy=#{lastUpdatedBy} ,lastUpdatedTime=#{lastUpdatedTime} ,"
			+ "firstName=#{firstName},"
			+ "lastName=#{lastName},picture=#{picture},preferredLanguage=#{preferredLanguage},"
			+ "backgroundCheck=#{backgroundCheck}"
			+ " where id=#{id}")
	public int update(User user);
	
}
